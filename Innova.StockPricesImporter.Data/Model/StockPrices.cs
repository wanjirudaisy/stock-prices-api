﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innova.StockPricesImporter.Data.Model
{
    public class StockPrices
    {
        public Guid StockPriceID { get; set; }
        public Guid StockID { get; set; }
        public DateTime MarketDate { get; set; }
        public decimal MarketPrice { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public Int32 SharesTraded { get; set; }
    }
}
