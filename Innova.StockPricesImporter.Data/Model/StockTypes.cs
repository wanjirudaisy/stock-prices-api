﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innova.StockPricesImporter.Data.Model
{
    public class StockTypes
    {
        public Guid StockTypeID { get; set; }
        public Guid StockExchangeID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Boolean Active { get; set; }
        public ICollection<Stocks> Stocks { get; set; }
    }
}
